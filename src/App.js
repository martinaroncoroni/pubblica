import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {color: 'red'}

    this.makeItBlue = this.makeItBlue.bind(this)
  }
  makeItBlue() {
    this.setState({color: 'blue'})
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">pizza mandolino to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <ComponenteDue
          color = {this.state.color}
          text = 'ciao ciao'
        />
        <Button
          makeItBlue = {this.makeItBlue}
        />
      </div>
    );
  }
}
//props:
//color : 'red' | 'yellow'
function ComponenteDue(props) {
  console.log(props)
  return(<div style={{backgroundColor: props.color}}>{props.text}</div>)
}

function Button(props) {
  return (<button onClick={props.makeItBlue}>cliccami!</button>)
}

export default App;
